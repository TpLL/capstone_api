class Product {
    constructor(
        id,
        name,
        price,
        screen,
        backCamera,
        frontCamera,
        img,
        desc,
        type
    ) {
        this.name = name;
        this.price = price;
        this.screen = screen;
        this.backCamera = backCamera;
        this.frontCamera = frontCamera;
        this.img = img;
        this.desc = desc;
        this.type = type;
        this.id = id;
    }
}

class Cart {
    constructor() {
        this.list = [];
    }
    add(item) {
        let index = this.list.findIndex((e) => {
            return e.id === item.id;
        });
        if (index === -1) {
            this.list.push(item);
        } else {
            this.list[index].quantity++;
        }
    }
    remove(id) {
        let indexRemove = this.list.findIndex((e) => {
            return e.id === id;
        });
        this.list.splice(indexRemove, 1);
    }
    getLength() {
        return this.list.length;
    }
}

class CartItem {
    constructor({ id, name, price, img, quantity }) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.img = img;
        this.quantity = 1;
    }
}
