function renderProduct(product) {
    let html = product
        .map((e) => {
            return `
            <tr style="height: 70px;">
                <td>
                    <div class="d-flex">
                        <img
                            src="${e.img}"
                            alt="..."
                            style="height: 50px; object-fit: contain"
                        />
                        <div class="ms-3">
                            <div class="text-center">
                                <h5 class="fw-bolder">${e.name}</h5>
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    ${e.price} $
                </td>
                <td>
                    ${e.desc}
                </td>
                <td>
                    <button class="btn btn-danger" onclick="deleteProduct('${e.id}')">Delete</button>
                    <button class="btn btn-primary" onclick="modifyProduct('${e.id}')" data-bs-toggle="modal"
                    data-bs-target="#modalForm">Modify</button>
                </td>
            </tr>
            `;
        })
        .join('');
    document.getElementById('productUI').innerHTML = html;
}

function getInfoForm() {
    let id = document.getElementById('pID').value;
    let name = document.getElementById('pName').value;
    let price = document.getElementById('pPrice').value;
    let screen = document.getElementById('pScreen').value;
    let backCamera = document.getElementById('pBackCamera').value;
    let frontCamera = document.getElementById('pFrontCamera').value;
    let img = document.getElementById('pImage').value;
    let desc = document.getElementById('pDesc').value;
    let type = document.getElementById('pType').value;

    return new Product(
        id,
        name,
        price,
        screen,
        backCamera,
        frontCamera,
        img,
        desc,
        type
    );
}

function showInfoToForm(product) {
    document.getElementById('pID').value = product.id;
    document.getElementById('pName').value = product.name;
    document.getElementById('pPrice').value = product.price;
    document.getElementById('pScreen').value = product.screen;
    document.getElementById('pBackCamera').value = product.backCamera;
    document.getElementById('pFrontCamera').value = product.frontCamera;
    document.getElementById('pImage').value = product.img;
    document.getElementById('pDesc').value = product.desc;
    document.getElementById('pType').value = product.type;
}

function showErrorValidate(errTag, err) {
    document.getElementById(errTag).innerText = err;
}

function resetForm() {
    document.getElementById('productForm').reset();
}

function sortDecrease(products) {
    for (let i = 0; i < products.length - 1; i++) {
        for (let j = i + 1; j < products.length; j++) {
            if (products[j].price > products[i].price) {
                [products[i], products[j]] = [products[j], products[i]];
            }
        }
    }
    return products;
}

function sortIncrease(products) {
    for (let i = 0; i < products.length - 1; i++) {
        for (let j = i + 1; j < products.length; j++) {
            if (products[j].price < products[i].price) {
                [products[i], products[j]] = [products[j], products[i]];
            }
        }
    }
    return products;
}
